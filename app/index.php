<!doctype html>
<html lang="fra">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Registre des pièces numériques</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="css/bare.min.css">
</head>

<body>
  <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->

  <!-- Add your site or application content here -->
  <form method="get" action="index.php" enctype="multipart/form-data">
  <section>
    <h1>Registre des pièces numériques</h1>
    <p>Donne la liste des fichiers numériques portant une certaine cote.</p>    
  </section>
  <section>
    <h2>1. Introduire la cote</h2>
    <p>On peut mettre la cote en version humaine (AMX 1/1/2) ou en version machine (AMX-001-001-002).</p>
    <label for="cote">Cote</label>
    <input type="text" id="cote" name="cote" <?php if (isset($_GET['cote'])) echo 'value="'.$_GET['cote'].'"'; ?>>
  </section>
  <section>
    <h2>2. Cliquez sur le bouton</h2>
    <input primary type="submit" name="submited" value="Consulter"/>
  </section>
</form>
<?php if (isset($_GET["cote"])): ?>
  <?php require('fonctions.php'); ?>

  <?php $liste = gen_liste($_GET["cote"]); ?>
<section>
  <h2>3. Résultats</h2>
  <?php if (sizeof($liste) == 0): ?>
    <p>Pas de références dans la base de donnée</p>
  <?php else: ?>
    <p><?php echo sizeof($liste)." document(s)"; ?></p>
    <?php if (in_array('digital',array_column($liste, 'objecttype'))): ?>
      <h4>Décompte par type de fichiers numériques</h4>
      <ul>
        <?php foreach(nb_types_doc_num($liste) as $mime => $nb): ?>
          <li><?php echo $mime." : ".$nb; ?></li>
        <?php endforeach; ?>      
      </ul>
    <?php endif; ?>
    <h4>Liste des documents</h4>
  <?php endif; ?>
  
  <ul>
    <?php foreach ($liste as $objet): ?>
    <li><?php echo mise_en_forme($objet); ?></li>
    <?php endforeach; ?>
  </ul>
  
</section>
<?php endif; ?>





</body>

</html>
