<?php

# !!! Pour récupérer les pièces, le user d'apache (www-data) doit être mebmbre du group à qui apartiennent les documents.
function gen_liste($cote)
{

require('.mongocredentials');

# Erreures de post

# initialisation des variables
$racine = "documents";

# Transformation des cotes humaine en cote machine si besoin
if(strpos($cote, ' ') !== false) {
	# changer cote humaine en cote machine
	list($fonds,$numeros) = explode(' ', $cote, 2);
	$numeros = explode('/', $numeros);
	foreach($numeros as &$numero) {
		$numero = sprintf("%03s", $numero);
	}
	$numeros = implode('-', $numeros);
	$cote = "$fonds-$numeros";
}

# Connexion Mango
$manager = new MongoDB\Driver\Manager($mongocredentials['uri']);

$query = new MongoDB\Driver\Query(['cote' => ['$regex' => "^$cote"]]);

$liste = $manager->executeQuery('fjme-db.objets', $query)->toArray();

# converti le tableau d'objets en pure tableau
# ATTENTION le tableau de résultat peut être trop gros et dépasser la memroy_limit de php.ini
# si c'est le cas, cela fait planter le script.
$liste = json_decode(json_encode($liste), true);

return $liste;

}

function mise_en_forme($ud)
{
	$text = "document mal décrit, vérifier la base de donnée";

	if (in_array("matériel", (array)$ud['objecttype'])) {
		$text = $ud['qte']." ".$ud['forme']." (".$ud['encodage'].") localisation : ".$ud['loc'];
	} elseif (in_array("digital", (array)$ud['objecttype'])) {
		$text = "(".$ud['mime'].") <a href='give_file.php?url=".$ud['localisation']."'>".$ud['filename'].'</a>'.
				" - ".$ud['taille']['largeur']['val']."x".$ud['taille']['hauteur']['val'].$ud['taille']['hauteur']['unite'].
				", ".$ud['taille']['volume']['val']." ".$ud['taille']['volume']['unite'];
	}

	if (isset($ud['notes'])) $text .= " -- ".$ud['notes']." --";

	return $text;
}

function nb_types_doc_num($liste)
{
	$mimes = array_count_values(array_column($liste, 'mime'));


	return $mimes;

}

?>
